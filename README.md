# Krom Automation Test



## Install playwright modules
```
npm i -D @playwright/test
npx playwright install
```
or 
```
npm init playwright@latest
```

## Add faker module
```
npm install --save-dev @faker-js/faker
```

## Running the test
```
npx playwright test --headed
```

