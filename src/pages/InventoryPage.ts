import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { CartPage, DetailPage, HomePage } from ".";
export class InventoryPage extends BasePage{
    async getFirstProductLocator():Promise<Locator>{
        const firstItem = this.page.locator('.css-1ne0r2x').first();
        return(firstItem);
    }

    async openSideBar():Promise<InventoryPage>{
        await this.page.locator('#react-burger-menu-btn').click();
        return this;
    }

    async clickLogout():Promise<HomePage>{
        await this.page.locator('#logout_sidebar_link').click();
        return new HomePage(this.page);
    }

    async addToCartByName(name: string):Promise<InventoryPage>{
        name = name.toLowerCase();
        name = name.replace(/ /g, '-')
        await this.page.locator(`#add-to-cart-${name}`).click()
        return this;
    }

    async openCart():Promise<CartPage>{
        await this.page.locator('.shopping_cart_link').click();
        return new CartPage(this.page);
    }

    async getCartCount():Promise<number>{
        const count = await this.page.locator('.shopping_cart_badge').textContent();
        return Number(count);
    }

    async openDetail(name:string):Promise<DetailPage>{
        await this.page.locator(`.inventory_item_name>>text=${name}`).click();
        return new DetailPage(this.page);
    }
}