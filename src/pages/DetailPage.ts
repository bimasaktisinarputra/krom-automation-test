
import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
export class DetailPage extends BasePage{
    async getName():Promise<Locator>{
        return this.page.locator('.inventory_details_name');
    }
}