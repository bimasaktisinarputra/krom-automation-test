import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { CheckOutPageTwo } from "./CheckOutPageTwo";
export class CheckOutPageOne extends BasePage{
    async fillFirstName(name: string):Promise<CheckOutPageOne>{
        await this.page.locator('#first-name').fill(name);
        return this;
    }

    async fillLastName(name: string):Promise<CheckOutPageOne>{
        await this.page.locator('#last-name').fill(name);
        return this;
    }

    async fillZip(zip: string):Promise<CheckOutPageOne>{
        await this.page.locator('#postal-code').fill(zip);
        return this;
    }

    async clickContinue():Promise<CheckOutPageTwo>{
        await this.page.locator('#continue').click();
        return new CheckOutPageTwo(this.page);
    }

    async getNotification():Promise<Locator>{
        return this.page.locator('[data-test="error"]');
    }
}