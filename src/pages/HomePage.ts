import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { InventoryPage } from ".";
export class HomePage extends BasePage{
    async fillUsername(username: string):Promise<HomePage>{
        await this.page.locator('#user-name').fill(username);
        return this;
    }

    async fillPassword(password: string):Promise<HomePage>{
        await this.page.locator('#password').fill(password);
        return this;
    }

    async clickLogin():Promise<InventoryPage>{
        await this.page.locator('#login-button').click();
        return new InventoryPage(this.page);
    }

    async getNotification():Promise<Locator>{
        return this.page.locator('[data-test="error"]');
    }

    async login(username: string, password: string):Promise<InventoryPage>{
        await this.fillUsername(username);
        await this.fillPassword(password);
        await this.clickLogin();
        return new InventoryPage(this.page);
    }
}