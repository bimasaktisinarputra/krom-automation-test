import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { InventoryPage } from "./InventoryPage";
export class CheckOutComplete extends BasePage{
    async getCompleteHeader():Promise<Locator>{
        return this.page.locator('.complete-header');
    }

    async clickBackHome():Promise<InventoryPage>{
        await this.page.locator('#back-to-products').click();
        return new InventoryPage(this.page);
    }
}
