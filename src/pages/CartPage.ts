import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { CheckOutPageOne, InventoryPage } from ".";
export class CartPage extends BasePage{
    async getCartQuantityByIndex(index:number):Promise<number>{
        const quantity = await this.page.locator('.cart_quantity').nth(index).textContent()||'';
        return Number(quantity);
    }

    async getCartQuantityCount():Promise<number>{
        return await this.page.locator('.cart_quantity').count();
    }

    async getCartQuantitySum():Promise<number>{
        const count = await this.getCartQuantityCount();
        let sum = 0;
        for(let i=0;i<count;i++){
            sum = sum+await this.getCartQuantityByIndex(i);
        }
        return sum;
    }

    async clickCheckout():Promise<CheckOutPageOne>{
        await this.page.locator('#checkout').click();
        return new CheckOutPageOne(this.page);
    }
}