import { Locator } from "@playwright/test";
import BasePage from "./BasePage";
import { CheckOutComplete } from ".";
export class CheckOutPageTwo extends BasePage{
    async clickFinish():Promise<CheckOutComplete>{
        await this.page.locator('#finish').click();
        return new CheckOutComplete(this.page);
    }

}