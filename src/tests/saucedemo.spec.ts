import {expect, test} from '@playwright/test'
import { HomePage } from '../pages';
import {faker} from '@faker-js/faker';
test.describe('saucedemo', ()=>{
  const username = ['standard_user', 'locked_out_user', 'problem_user', 'performance_glitch_user', 'error_user', 'visual_user']
  const password = 'secret_sauce';
  const item = ['Sauce Labs Backpack', 'Sauce Labs Bike Light', 'Sauce Labs Bolt T-Shirt', 'Sauce Labs Fleece Jacket', 'Sauce Labs Onesie', 'Test.allTheThings() T-Shirt (Red)']
  test.beforeEach(async ({page, baseURL}) => {
    await page.setViewportSize({width:1920,height:1280});
    if (!baseURL) throw 'baseURL empty';
    //scenario 1
    await page.goto(baseURL);
    await page.waitForURL('**\/');
    await expect(page).toHaveURL(/.*/);  
  }); 

  test('user able to login using correct credential', async({page})=>{
    const homePage = new HomePage(page);
    await homePage.fillUsername(username[0]);
    await homePage.fillPassword(password);
    await homePage.clickLogin();
    await expect(page).toHaveURL(/.*inventory.html/)
  })

  test('user unable to login with wrong credential', async({page})=>{
    const homePage = new HomePage(page);
    await homePage.fillUsername('wrongusername');
    await homePage.fillPassword('wrongsecret');
    await homePage.clickLogin();
    const notif = await homePage.getNotification();
    await expect(notif).toContainText('Epic sadface: Username and password do not match any user in this service');
  })

  test('user unable to login if locked out', async({page})=>{
    const homePage = new HomePage(page);
    await homePage.fillUsername(username[1]);
    await homePage.fillPassword(password);
    await homePage.clickLogin();
    const notif = await homePage.getNotification();
    await expect(notif).toContainText('Epic sadface: Sorry, this user has been locked out.');
  })

  test('user able to logout', async({page})=>{
    const inventoryPage = await new HomePage(page).login(username[0],password);
    await inventoryPage.openSideBar()
      .then(_=>_.clickLogout());
    await expect(page).toHaveURL(/./);
  })

  test('user able to see same number of inventory in cart', async({page})=>{
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const numberOfItem = faker.number.int({min:1, max:6});
    for (let i=0;i<numberOfItem;i++){
      await inventoryPage.addToCartByName(item[i]);
    }
    const count = await inventoryPage.getCartCount();
    expect(count).toEqual(numberOfItem);
  })
  
  test('user unable to checkout if the Zip is empty', async({page})=>{
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const checkOutPageOne = await inventoryPage.addToCartByName(item[0])
      .then(_=>_.openCart())
      .then(_=>_.clickCheckout())
    await checkOutPageOne.fillFirstName(firstName);
    await checkOutPageOne.fillLastName(lastName);
    await checkOutPageOne.clickContinue();
    const notif = await checkOutPageOne.getNotification();

    await expect(notif).toContainText('Error: Postal Code is required');
  })

  test('user unable to checkout if the Last Name is empty', async({page})=>{
    const firstName = faker.person.firstName();
    const zip = faker.location.zipCode();
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const checkOutPageOne = await inventoryPage.addToCartByName(item[0])
      .then(_=>_.openCart())
      .then(_=>_.clickCheckout())
    await checkOutPageOne.fillFirstName(firstName);
    await checkOutPageOne.clickContinue();
    const notif = await checkOutPageOne.getNotification();

    await expect(notif).toContainText('Error: Last Name is required');
  })

  test('user unable to checkout if the First Name is empty', async({page})=>{
    const lastName = faker.person.lastName();
    const zip = faker.location.zipCode();
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const checkOutPageOne = await inventoryPage.addToCartByName(item[0])
      .then(_=>_.openCart())
      .then(_=>_.clickCheckout())
    await checkOutPageOne.fillLastName(lastName);
    await checkOutPageOne.fillZip(zip);
    await checkOutPageOne.clickContinue();
    const notif = await checkOutPageOne.getNotification();

    await expect(notif).toContainText('Error: First Name is required');
  })

  test('user able to checkout', async({page})=>{
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const zip = faker.location.zipCode();
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const checkOutPageOne = await inventoryPage.addToCartByName(item[0])
      .then(_=>_.openCart())
      .then(_=>_.clickCheckout())
    await checkOutPageOne.fillFirstName(firstName);
    await checkOutPageOne.fillLastName(lastName);
    await checkOutPageOne.fillZip(zip);
    const checkOutPageTwo = await checkOutPageOne.clickContinue();
    const checkOutComplete = await checkOutPageTwo.clickFinish();
    const notif = await checkOutComplete.getCompleteHeader();
    await expect(notif).toHaveText('Thank you for your order!');
  })

  test('user able to see correct detail of item', async({page})=>{
    const inventoryPage = await new HomePage(page).login(username[0],password);
    const numberOfItem = faker.number.int({min:1, max:6});
    const detailPage = await inventoryPage.openDetail(item[numberOfItem]);
    const name = await detailPage.getName();
    await expect(name).toHaveText(item[numberOfItem]);
  })
})